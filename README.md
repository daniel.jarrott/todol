# todol: ToDo api in Laraval
 
A simple REST API for a todo list application. The todo list allows the user to
manage a list of tasks.

Each task consisting of a title, date and whether or not it is completed. Each task can be
assigned to one or more users, the user should have an email and password field.

The API is written in PHP using the Laravel Framework.

## Current features

 - Return a list of tasks [GET tasks/]
 - Add a task [POST tasks/] {'title': 'test1', 'date': '2019-11-22', 'completed': false}
 - Delete a task [DELETE tasks/{taskId}]

## Planned features

 - Mark a task as complete
 - Edit a task

## Key points

 - a Laravel (Lumen) app in 'todol/api'
 - DB migrations in 'todol/api/database/migrations'; and dump in todol/api/database/dump.my.sql
 - Partial API implementation in 'todol/api/routes/web.php' (missing edit and marking complete)
    - Quick working implementation from route to DB; next iteration add controller and model.
 - Test cases in 'todol/api/tests/TasksTest.php' 

## TODO

 - implement the task edit and complete features
 - dockerise
 - move the task implementations out of routes and into a controller with a supporting model
 - implement user features using Laravel builtins: 
    - https://laravel.com/docs/5.8/authentication
    - https://laravel.com/docs/5.8/api-authentication
