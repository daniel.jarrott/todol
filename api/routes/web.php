<?php

use Illuminate\Http\Request;

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('tasks', function () {
    $results = app('db')->table('tasks')->select("*")->get();
    return response()->json($results);
});

$router->post('tasks', function (Request $request) {
    $title = $request->input('title');
    $date = $request->input('date');
    $completed = $request->input('completed');
    $id = app('db')->table('tasks')->insertGetId(['title' => $title, 'date' => $date, 'completed' => $completed]);
    return response()->json(['result' => 'created', 'task_id' => $id]);
});

$router->put('tasks/{taskId}', function ($taskId) {
    return 'TODO edit ' . $taskId;
});

$router->delete('tasks/{taskId}', function ($taskId) {
    app('db')->table('tasks')->where('task_id', '=', $taskId)->delete();
    return response()->json(['result' => 'deleted', 'task_id' => $taskId]);
});

