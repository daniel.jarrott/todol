<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class TasksTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Test that adding and listing.
     *
     * @return void
     */
    public function testAddAndList()
    {
        $this->get('tasks');

        $this->assertEquals(
            '[]', $this->response->getContent()
        );

	$this->post('tasks', ['title' => 'test1', 'date' => '2019-11-22', 'completed' => false])
            ->seeJson([ 'task_id' => 1 ]);

        $this->get('tasks')
            ->seeJson([ 'task_id' => 1, 'title' => 'test1', 'date' => '2019-11-22']);
    }

    /**
     * Test deleting.
     *
     * @return void
     */
    public function testDelete()
    {
	$this->post('tasks', ['title' => 'test1', 'date' => '2019-11-22', 'completed' => false])
            ->seeJson(['task_id' => 1]);

        $this->get('tasks')
            ->seeJson(['task_id' => 1, 'title' => 'test1', 'date' => '2019-11-22']);

	$this->delete('tasks/1')
            ->seeJson(['result' => 'deleted']);

        $this->get('tasks');

        $this->assertEquals(
            '[]', $this->response->getContent()
        );
    }
}
